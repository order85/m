var rule = {
author: '小可乐/240701/第一版',
title: 'Cally',
类型: '影视',
host: 'https://cally66.icu',
hostJs: '',
headers: {'User-Agent': 'MOBILE_UA'},
编码: 'utf-8',
timeout: 5000,

homeUrl: '/',
url: '/vod/list.html?page=fypage&type_id=fyclassfyfilter',
filter_url: '&{{fl.area}}&{{fl.class}}&{{fl.lang}}&{{fl.year}}',
detailUrl: '',
searchUrl: '/public/auto/search1.html?keyword=**&page=fypage',
searchable: 1, 
quickSearch: 1, 
filterable: 1, 

class_name: '电影&剧集&综艺&动漫&短剧&更新',
class_url: '1&2&3&4&39&todayup',
filter_def: {},

play_parse: true,
parse_url: '',
lazy: `js:
let ctid = input.match(/line_id=(\\d+)/)[1];
let jurl = 'https://cally66.icu/openapi/playline/' + ctid;
let html = request(jurl, {headers:{"User-Agent":MOBILE_UA}});
let kurl = html.match(/file":"(.*?)"/)[1];
input = { jx: 0, parse: 0, url: kurl }
`,

limit: 9,
double: false,
推荐: '*',
一级: '.module-item:has(.module-item-note);img&&alt;img&&data-original;.module-item-note&&Text;a&&href',
二级: {
title: 'h1&&Text;.module-info-item-content:eq(-1)&&Text',
img: '.module-info-poster&&img&&data-original',
desc: '.module-info-item-content:eq(2)&&Text;.module-info-tag-link:eq(0)&&Text;.module-info-tag-link:eq(1)&&Text;.module-info-item-content:eq(1)&&Text;.module-info-item-content:eq(0)&&Text',
content: '.show-desc&&Text',
tabs: '.tab-item span',
tab_text: 'body&&Text',
lists: '.module-play-list:eq(#id)&&a',
list_text: 'body&&Text',
list_url: 'a&&href'
},
搜索: '*',

filter: 'H4sIAAAAAAAAA+2Y30/aUBTH3/kz+gxJCwXBZH+JIUvjyLLM+aBuiTEkKgMB55yLwhQSlw0HOJ2w+ZNa/Gd6e9v/Yq1jnuvpefCB8HQfez/f3kvP95wvTVciiqZMz0RWlNe5ZWVamZ0zFheVqDJvvMn5l6xc4n3Tv35nzL31F2ZWlPlgudjxCp1g2b9Q8tHRarVjW01e2RiBWWMp9/zVi2cZUNSarNIOKzT1QcLLfadQJCTag8RZ++Ss1ghJHA6qtOldEnBQe4fdmIREh4MKVWf9gJAk4aCjDXqXlFgWvktJpuCg9RNe2yEkadil9Nnb7xISqC4fHDNrLyyJQ3Wd8hd7UCEkUF1ubrPidViiCwdVz7h1HJYkBRvXht7BkJAIB1k73Gzy3Qtm9bHKdzKbjz505XLOWBCa8ubcNq0nNmVcjeujtWCX+4cVYQLBhAjjCCZFqCGYEqGKIHitJWO+Z48p2KypMQ3/Xii8moyp+F5hdlQ1puKbxblpnDv1Luao1MZCzhBK3eyxD4Onzn+r7e2XRqvBPvfFhmo712dsu4c5FNypHznNE8xhHL3DLmtYmIMnfPiR4GCL86dHcHDG3ewTPC3cv0dwId2GV2GeAHvYVo8NfmAO/th374n7hfqdtP1HxBzqx4qX7FcBc/2xv3PG/Evw1z07dTurT/W3Yfn60WqwD4pLv34EF/z53SJ4SvSf4FOi/wQHf7y6xbbqmGfo+vznQThm85FsNKLEx/ZH2LDsQdv/D8Ipi2eBkCSQ3YQkKVYsGIqwBDc9IUmLdbHNGiHJyBSWKSxTWKbwJFM4MbYUvh8Ebt66lQFKNqEd/o0DrdLEovjPTatwa9AqYcAurlirRqt0mbgycWXiysSdZOLqY37vrXac25842UIvraQqhUynVVPo0WhVWmapzFKZpTJLJ/r2mhlXmPLunXtZRaGmQyd4G1t89xBz6AS23XK/FzGHTrDNfXv4FXPoBK9gset1zMEpXia+KuhCzK9t8jL+sKuDU95qyf12ijmOdsxlqE821CPZSP4v8w0GSiQaAAA='
}