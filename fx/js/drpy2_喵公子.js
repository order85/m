var rule = {
author: '小可乐/240701/第一版',
title: '喵公子',
类型: '影视',
host: 'https://order11.serv00.net',
hostJs: '',
headers: {'User-Agent': 'MOBILE_UA'},
编码: 'utf-8',
timeout: 5000,

homeUrl: '/',
url: '/index.php/vod/show/id/fyfilter.html',
filter_url: '{{fl.cateId}}{{fl.area}}{{fl.by}}{{fl.class}}{{fl.lang}}{{fl.letter}}/page/fypage{{fl.year}}',
detailUrl: '',
searchUrl: '/index.php/vod/search/page/fypage/wd/**.html',
searchable: 1, 
quickSearch: 1, 
filterable: 1, 

class_name: '电影&电视剧&综艺&动漫&短剧',
class_url: '1&2&3&4&5',
filter_def: {
1: {cateId: '1'},
2: {cateId: '2'},
3: {cateId: '3'},
4: {cateId: '4'},
5: {cateId: '5'}
},

play_parse: true,
lazy: `js:
let kcode = JSON.parse(request(input).match(/var player_.*?=(.*?)</)[1]);
let kurl = kcode.url;
if (/\\.(m3u8|mp4)/.test(kurl)) {
input = { jx: 0, parse: 0, url: kurl };
} else {
input = { jx: 0, parse: 1, url: kurl };
}
`,

limit: 9,
double: false,
推荐: '*',
一级: '.module-item;img&&alt;img&&data-src;.module-item-text&&Text;a:eq(0)&&href',
二级: {
title: 'h1&&Text;.tag-link:eq(1)&&Text',
img: '.lazyload:eq(-1)&&data-src',
desc: '.video-info-item:eq(4)&&Text;.tag-link:eq(-2)&&Text;.tag-link:eq(-1)&&Text;.video-info-item:eq(1)&&Text;.video-info-item:eq(0)&&Text',
content: '.vod_content&&span&&Text',
tabs: '.tab-item span',
tab_text: 'body&&Text',
lists: '.sort-item:eq(#id)&&a',
list_text: 'body&&Text',
list_url: 'a&&href',
},
搜索: '.module-search-item;.video-serial&&title;*;.video-serial&&Text;.video-serial&&href',

filter: 'H4sIAAAAAAAAA+2aW1MTSRTH3/kYeWYrMwG8vXm/3+9aPkQ3tWst61YJu1WUZRUKiQkqAQoTWQLICgSVQEBESAx8mfRM8i12ktM5ffoMuxkL3PVhHvP/HU5Pn+5pzn9mHrUEQoEDt1seBX6J9AQOBO6FuyMnfwy0Bh6Ef404v+3loph87vz+I9z5uyPcfhR44MgiOl/tm6/Jzo9Q4HEryHZ82eqL2olnDWIgEgPz5VKGIBORFX9dLiQIUgkri3lRekVQm0qYyohEViFTITs7LDaKBLWrsZ4MWb0pgjpUwkRWu3hzj0LJmcpbgtqdhHcet6qqdYa7ulTRINO/F43NQ6rBeqag1PSy6iFS0+elh0hNXwE2EGh64dhAoOlVYllA09eTXQtouK65OfHivR4iNbyWgSW7xEKkRmZkjxZdM6ppGDL7zDUjqeHl5ubKm2/Y5YKGWWIj1bF3LAtomGXyvTNHlgU0DHm6YKeGWQhoGNI3YD39k4WAhnUpJkV0ndUFtEZIdWLEej2rh0gNB0o9qyQKbCDQcNKbi/boJ1FaZvNGWb87WBRoGDIYE8kVFgIabomtIWft2JYATS1DxpoY5stQ1zCkf8v+wKYuNSxgadguZrabmka0Gzz8MBIm93cmL14UvN7fM9nqWKwxUC1RUEq4XHNj1vqSFiElVeG8tbGp5wAJJ7U5KMZLWoSUcMU/vuIRUsIVeL7MI6SEOdKzVmZBzwESzmXqHc8hJbWtPvMIKakrzbuvNK/leJkXhTk9B0iYoz/pVFnE3+lpUMU5z27ZyZydGNOnjao6at5Yz7ecP9YHRRXjomvlYkoPAknbT53hBz+p/VRZylXme73up/GSE98YoZYoKCWyjjxCSrhbVmZ4hJRwHdMl8TLNg5RK1tsVBBLZMzxCSmRnuiJAInvGNWeQSN3FYp8eAZJW955I+KGqu5Veq6ZXPdY9ZITaG/lraYJ1gdA2TtsoDXEaotTk1KTU4NQg1NzPqCMQuo/TfZTu5XQvpXs43UNpB6cdlPJambRWJq+VSWtl8lqZtFYmr5VJa2XyWpm0VgavlUFrZfBaGbRWBq+VQWtl8FoZtFYGr5VBa2XwWhmsveyMdHdHyL4VubS19NLjvj2IN0U9S/AgkkOMHEJymJHDSI4wcgTJUUaOIjnGyDEkxxk5juQEIyeQnGTkJJJTjJxCcpqR00jOMHIGyVlGziI5x8g5JOcZOY/kAiMXkFxk5CKSS4xcQnKZkctIrjByBclVRq4iucbINSTXGbmO5AYjN5DcZOQmkluM3EJi/LCfsZqi3QN3e8i5PTgiCknX/lfHeS3R3Z5g930nvDFGuVCw8qOE/ny/u0v9z1zqF/EYoV33fnsYqV1Cy53WlkDbTj2yOt+gtVOWL6QOCuhyFGpT55716bOYSRFE7HO9AyNInXjQepGx1HFnLWRrDaJC6qxz/qRcyBKkjm7oOgnau4tGuLmN8OAsweeIJ2uiL7mdA5LkK9y3WFwThRwLAe3rvGoz9+3Bq3pw3x6slgcLWd6YdlktqSmvGrXGlthigIbX8irmMsVSI27MtQBS276nllm2aap3YtJiUSdeb9tB8m5sdsPGNTdYXoxeM/vkweil8o63ERPTehpUfbujgr7W7vhWxbcqvlXxrYpvVXyr8g2sSsdOrYq638Ej2MUv6lVBO7Ek6Vnnf59OiStxPE4yr1PyNrD+BFyjbft3z0pUexN2tpe1/6DR7rV/yt29OhoWenGrko/rIVLDLCM5a4C9bpCaKlPUWmfNttRUV/GxvDHELhc00vZV37LLlRqGFN6LxUkWAhpey/iK+60PaJhldMpa5a8AQVNLu27Fk+XCiOt9jUawjKt/OeaClRE0zLj8tPLkBcsF2n/X4cOO1RtekPQdz7vzmqTfMXoESH4/jBF+P+z3w34/7PfDfj/s98Pfvh9u32k/TD4eg4fwA/PWlw+NrlWdM9As6FSdM/J5u0bJk/96G6FT8mUaPJDX6C5+geahFfbwYZjdl6tMs55bajjQ4Lw9FGMDgYYhQ5P2Av9GCjTVyTX9GKsyNFEZZO8JpIYDvZkW4+ypvtRwoOaP7K1Mwf3VF2h4Lc2/a/LwWkPknWKvsmsBjYbMfnSHOBqu0cxm+Qv7vExqmGVwSsTHWRbQ1O24InLMrkgNBxofsMaY0ZCaqu6y2Erz6tY10hl+8wf/9VtLtwUgkVuzySN5D8bhn23BtlP1H6T7xsE3Dr5x8I2Dbxx84/B/GQfto5/v67+If1L4J4V/UnwXJ0XL478BNplnb0Q3AAA='
}