var rule = {
author: '小可乐/2408/第一版',
title: '影探app',
类型: '影视',
host: 'http://cms-yt.lyyytv.cn',
hostJs: '',
headers: {'User-Agent': 'MOBILE_UA'},
编码: 'utf-8',
timeout: 5000,

homeUrl: '/api.php/app/index_video',
url: '/api.php/app/video?tid=fyfilter&pg=fypage&limit=20',
filter_url: '{{fl.cateId}}&{{fl.class}}&{{fl.area}}&{{fl.lang}}&{{fl.year}}',
detailUrl: '/api.php/app/video_detail?id=fyid',
searchUrl: '/api.php/app/search?text=**&pg=fypage',
searchable: 1, 
quickSearch: 1, 
filterable: 1, 

class_name: '电影综合&影视剧综合&综艺综合&动漫综合&热门短剧',
class_url: '1&2&3&4&61',
filter_def: {
1: {cateId: '1'},
2: {cateId: '2'},
3: {cateId: '3'},
4: {cateId: '4'},
61: {cateId: '61'}
},

play_parse: true,
lazy: `js:
if (/\\.(m3u8|mp4)/.test(input)) {
    input = { jx: 0, parse: 0, url: input };
} else {
let kurl = 'https://cdn.suxun.site/api/?type=xh&key=4Tb7dBOiAiKHf7AIsb&url=' + input;
kurl = JSON.parse(request(kurl)).url;
input = { jx: 0, parse: 0, url: kurl }
}
`,

limit: 9,
double: true,
推荐: 'json:list;vlist;*;*;*;*',
一级: 'json:list;vod_name;vod_pic;vod_remarks;vod_id',
二级: `js:
let kdata = JSON.parse(request(input)).data;
VOD = kdata;
VOD.type_name = kdata.vod_class;
`,
搜索: '*',

filter: 'H4sIAAAAAAAAA+1az1MiRxS++2dw9jADiLpVXrZySeWaymVrD1Ri5RDjYTdJ1ZZlFcqCwLr8MEY0gGhWhTWCoK5BEPxn6Jnhv0gP3fO6X8NmqGA2Vckc5/s+3nvzeuj30cPajE/3PXk2s+b7bvmV74nv6/APy59/45v1rYa/X6bXZrNDDt/Q65/CKz9S4Nmab5XCJFYdRKs2TC903/osg429hrn7gXSbwS8459eANKO1Qb6q8PMB4AebH4xGxopsKJI5UJDTrpVuktKhzqng/CjndyL7R7mAw+mjXNDhRD6jdUkyDbOTIbEWpFwYS0NWbSztJA6N/7STO7Qo6jqpDA7iOPecPpZ2coekbhS6/XYFegnFBz+mgBjS3e+njf1yv/Wz1cs5n5fYjawR2TOTW+ZVbxBJOhWKprO4ZueeZBNO/tBY1skdoPU/X58Vz+JK+OVL8SiSZMWIxv76URS3t1ekeo4OAy1xCCpINHk4UHAI3yJScAiypKr9bhFnYRBkqeTIXQdnYRDEgNsSMRgEdST2++0kroNBjsKqnZHtc6TgENSRujS7WMEhyFI76/eOcBYGQaXxncHBe1wpgyDG4TmtDMdgECg2L8y9HFYwCBTRlLH5K1YwCDaK0o6xf4oUHBL70JaVbOMYDIJ76dXZI4hvB1DQZU6sd8rqMAgU6TjJXGEFg2B1HrL0qcCrwyDR2aJRyimdHULoCxF+sRyWvg/FBtluT/p9GO4XHLXjLHEEmnp2QHckWcAR0YmGcddDERgCj1gvTTcVWcARWJX8qVG8kAUcgRrK75UIHBGr9oci4AikuG4oAo5AhLcN0j5DERgiliJGG4sEDIHVfNNUUnBEGoIkdURKx6gMACFR7Lbf2UOJGIKWfCW8+q1YcuuyZlUjky55oUv1HLXjLHFEuhNFwBFY0asTRcARWLB8l7zNKxoBSuuqahgiPRmKgCNSs0g9iu6FIahZr5bDL0SzjPztIH8zYbP8mt8ZwnaUpeG1RAYUMiCTfoX0y6SukLpMagqpSaS+iEl9USYXFHJBJucVcl4mQwoZksk5hZyTSaVDutwhXemQLndIVzqkyx3SlQ7pcod0pUP0mi75zPPZGZ9/Wt/qR1/ZZEWYVlEBM62UHBTiwIdC8peMkmahYLs4hw8K3ohEjHzly6+eOgYpID/09vaWrDz9zIkrmceLir17SuSc5A27TasSt/MidyU9d4rCab/92D6aw3KdjO6+hY1tsnFLopkx85wTk3s6Ur8l7RpWMGhyH+Tu6cjplurpODS5p3N3Dczb40oZ5Cj6d8eqa+CQ8FIx4+ASrwCDoI5f4qpj4xDU0c2pXefQ2EnGY4wZZdO4F7eh7Oocprc3ru7F3f+4mRPPWnjWwrMW/6a1AFJTOqTJHdKUDmlyhzSlQ5rcIU3pkCZ3SFM6pMkd0pQO0WvHCAWmNUKin/yMrnNPfysLuyMd6gwZxXYsPJ6pGESSZiWCrQCD5JH2ujwy0igEe0/9wWokkIJDEGOnZqTwAQKHxK4RM1p4+HJIbDzX/bssrpRB0jwYvMOVcggU7XNSP8QKBkEdhauRgwwGQYzdsnGjHDAxCGK0WkYi02/vqIcQiIDu3fxGbQbuHoMgXnPT2tjGkRj0ycY9OztFQ5Ah2Fsrw9pGsL9GAoZ4I9Ibkd6I/K+NyOC0IxK9ODHufxdvp8SNkUaOvH6gm6m52xG8aBll6CeV4Rl8vOHpPhrd30ZQA2Ad4wHMIciSrprZOM7CIFBkD80L5YyfQWKDd3uTYGVLVhqfHXAIshwdkwL+rc+hyX/JG8X2yPsKBkEdrmf37qcc9nNxd4PrYJCsOL0eUVAI1uWk17/Hb0U4JE4UyiRRwDEYBOcF7StSw5aFQ5ClkDIOsN3gkOhpkzzklZ4OoU94GjB8dYnsAUMm/qnu7h8+ag+8n9ief/D8w//NP4Sm/pNMSPrvRq9ulmvyC4f5R3QB3rm8rPDO5f/RSeydy7MI3rm8Zxr+vmmwh8zM+p9dYrSDiykAAA=='
}