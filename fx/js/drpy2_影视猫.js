var rule = {
author: '小可乐/240701/第一版',
title: '影视猫',
host: 'http://ysm.tkbot.fun',
hostJs: '',
headers: {'User-Agent': 'MOBILE_UA'},
编码: 'utf-8',
timeout: 5000,

homeUrl: '/',
url: '/index.php/vod/show/id/fyclassfyfilter.html',
filter_url: '{{fl.area}}{{fl.by}}{{fl.class}}{{fl.letter}}/page/fypage{{fl.year}}',
detailUrl: '',
searchUrl: '/index.php/vod/search/page/fypage/wd/**.html',
searchable: 1, 
quickSearch: 1, 
filterable: 1, 

class_name: '电影&剧集&综艺&动漫&短剧&TV直播',
class_url: '1&2&3&4&6&7',
filter_def: {},

play_parse: true,
parse_url: '',
lazy: `js:
if (/\\.(m3u8|mp4)/.test(input)) {
    input = { parse:0, url:input };
} else if (rule.parse_url.startsWith('json:')) {
    let kurl = rule.parse_url.replace('json:','') + input;
    kurl = JSON.parse(request(kurl)).url;
    input = { parse:0, url: kurl };
} else {
    input= rule.parse_url + input;
}`,
    
limit: 9,
double: false,
推荐: '*',
一级: '.module-item:has(.module-item-note);img&&alt;img&&data-original;.module-item-note&&Text;a&&href',
二级: `js:
input = input.match(/id\\/(\\d+)/)[1];
input = HOST + '/api.php/provide/vod/?ac=detail&ids=' + input;
let kjson=JSON.parse(request(input));
let data=kjson.list;
VOD=data[0]
`,
搜索: '.module-card-item-poster;*;*;*;*',

filter: 'H4sIAAAAAAAAA+2a2U4bSRSG73kMXxPZLGHJXfZ9X0mUCyeyNNEwGQmYkRBCAowdY8ALAjsezI4xEAw2MIyxY/wyrl7eIm1X9emq05bcUTzJTNQ3SP7+n1NVXeWu39U90uJoc1x41TLi+NUz7LjgeNvvHhx0tDreu3/zaB/JVFry+rTPf7r7/9DAqxHH+yr2bave7SrWPjhGWxmNJTU/o85aJSdjukUO5Fg9w8KYbpHGI9JYTLQwBg0FtyulJGqIMmgoHSWnRdQQZVAFxsZVoQz6EvhYKUyhvlCmW5TMFpnZFS2MQV+CB3IJWRjjRiTPF00jqjKwpD6YRsQYdDezVTlbRd2lDKr459TEDqpCGVRZ3tXGiKpQBpaJPTkWRRbKwOINShN/IQtlcF2KYeLLo+tCmW5Rl+akjynRwhg0FPugTBVQQ5TBoM/25fm/SSmHxg0YjOFNZQMvCcrAEvKT8CGyUAZLohzR5g4tCcqMaUhKS1E8DTUGlsmy/AkNnTG4gKWoXEzWG5qgjL4ebYUvuHvA4+a+38ksmSlY/X5vptWEX2+oWsjJEEzXVkLKHwgOhowrnJVOz8QaFMGgzkJksSQ4GIIZP1rADoZgBqZz2MEQ1IinpOSeWIMiGMvKDq7BkLGs/sEOhoyeZs09zQo1ZrOksCXWoAhqTIa1q0wCO2IZoDDmVFkOZ+SphDhsoMatZlWaLmv/LDYKFHy+k0oxJpooEtbTsMc9YKwnKX6ixo8trqd2V3unXr9axlkDnNqB1Q5ebcdqO6+2YbWNV11YdXFqWy9SNcCpPVjt4dVurHbzahdWu3j1/DntD+6aTg2fq1cjrvPIp1PO11kluJ5OdV+vi5weV4obgg2g7uqp5+rBLnHN1Dz11ky/Z2jIw60akolLB7MWV81FvTyt4rwIyiWkXALlMlIug3IFKVdAuYqUq6BcQ8o1UK4j5TooN5ByA5SbSLkJyi2k3ALlNlJug3IHKXdAuYuUu6DcQ8o9UO4j5T4oD5DyAJSHSHkIyiOkPALlMVIeg/IEKU9AeYqUp6A8Q8ozUJ4j5TkoL5DyApQ+pPSB8hIpL0FxnetFWpUI34E3w9xdMzRHCmHT+jduptVCb4adQ+80u95GpVCQsvOc+su7oUFjNziYJAE/pw6+/X3AU+1Cy+vWFkd70zJ/48RkIUTTSEfGT4g3XC/sMeUrfmiQ/RNSyCALZV8Xyxv90LAQyy380LCQKi2k5crpmilVMmbEcp+UOECTQRn0ZcFvyv+MccHTNAGM1d8KWJU6e8G35FG/T/OLCYUi6xmuGYm1cZa0kmkbJUULmTaW1WIcWVoTywC1k52d7OxkZyc7O9n9xMmuo1nJTh2bktNjKI1RxoeJyRVzmNAY9Ha/rGQDooUxqDKXkYLooIsxY4f1SXmUfRgztvqjymkEdZcybhdWN1B3GQNLYZfsLyMLZdCXxUPzeSNlUGV+RTrGh8+UQZV8XgqEK4U500mhoMBlPF7Xsh66jJRBxdyEMj6DalH2HQPXd4lTjWOdtJfWZlasQZEdceyIY0ccO+LYEed/G3E6mxVxLOQXC8+RZW9GWUNBiTFoKLQtR/yoIcrAElmW9/AjVcqM7bfhs1slsqSE0FkbY9DQ6hpZRCdjjEFDjY+9pGTB/JCYMuhL48egFo4GSVa72MeoL5TxltSR2aIxmKPNs8pn9DSaMagSWiGBRVSFMmPVHpIMypiMQUOLQSmB0iFjxtXNkXIcX90aq78V/aDDs385h9Udqn0YZSc1O6nZSc1Oaj9FUutqVlKTQ1ntBqB6SyQ/gTZXXoFuTUal4+V6b6kJCtxParcXeaesnATRpssrxhFG9flmpZgwvWwnKIZ9VintyYGS+Zklr4A9daSuR6XxaTmA31fjFSNCRdSFsjrmV9ZxqOMV+x0w+x0w+x2wbwxndriww4UdLv4r4aK7ae8wbe4raXQ6wxhsSMlxbYtCyYMyqJKfMFkYgwElQty9mQUSyuBOH4uruc9oI6cMLMGYuoKOOxiDu4j8CTVTI7Ac+kSxz1gPVk8efuDUt4x+AdIlAr21MgAA'
}