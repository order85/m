var rule = {
author: '小可乐/240516/第一版',
title: '星辰影院',
host: 'http://www.xingchenwu.com',
hostJs: '',
headers: {'User-Agent': 'MOBILE_UA'},
编码: 'utf-8',
timeout: 5000,

homeUrl: '/',
url: '/fyfilter/indexfypage.html[/fyfilter/index.html]',
filter_url: '{{fl.cateId}}',
detailUrl: '',
searchUrl: '/search.php?page=fypage&searchword=**&searchtype=',
searchable: 1, 
quickSearch: 1, 
filterable: 1, 

class_name: '电影&剧集&综艺&动漫',
class_url: 'dianying&dianshiju&zongyi&dongman',
filter: {
"dianying":[
{"key":"cateId","name":"分类","value":[{"n":"全部","v":""},{"n":"动作片","v":"dongzuopian"},{"n":"爱情片","v":"aiqingpian"},{"n":"科幻片","v":"kehuanpian"},{"n":"恐怖片","v":"kongbupian"},{"n":"喜剧片","v":"xijupian"},{"n":"剧情片","v":"juqingpian"},{"n":"在线直播","v":"zaixianzhibo"}]}
],        
"dianshiju":[
{"key":"cateId","name":"分类","value":[{"n":"全部","v":""},{"n":"国产剧","v":"guochanju"},{"n":"港台剧","v":"tangtaiju"},{"n":"欧美剧","v":"oumeiju"},{"n":"日韩剧","v":"rihanju"}]}
]
},
filter_def: {
dianying:{cateId: 'dianying'},
dianshiju:{cateId: 'dianshiju'},
zongyi:{cateId: 'zongyi'},
dongman:{cateId: 'dongman'}
},

proxy_rule: '',  
sniffer: 0,
isVideo: '',
play_parse: true,
lazy: '',

limit: 9,
double: false,
推荐: '*;*;*;*;*',
一级: '.stui-vodlist li;a&&title;a&&data-original;.text-right&&Text;a&&href',
二级: {
//名称;类型
"title": "h1&&Text;.data:eq(3)&&a:eq(0)&&Text",
//图片
"img": ".v-thumb&&data-original",
//主要描述;年份;地区;演员;导演
"desc": ".data:eq(0)&&Text;.data:eq(3)&&a:eq(2)&&Text;.data:eq(3)&&a:eq(1)&&Text;.data--span:eq(2)&&Text;.data--span:eq(1)&&Text",
//简介
"content": ".detail-content:eq(0)&&Text",
//线路数组
"tabs": ".bottom-line:has(span)&&h3",
//线路标题
"tab_text": "body&&Text",
//播放数组 播放列表
"lists": ".stui-content__playlist:eq(#id) a",
//播放标题
"list_text": "a&&Text",
//播放链接
"list_url": "a&&href"
},
搜索: '.stui-vodlist__media:eq(0) li;*;*;*;*'
}