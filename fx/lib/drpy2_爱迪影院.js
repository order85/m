var rule = {
author: '小可乐/240601/第一版',
title: '爱迪影院',
类型: '影视',
host: 'https://www.idiitv.com',
hostJs: '',
headers: {'User-Agent': 'MOBILE_UA'},
编码: 'utf-8',
timeout: 5000,

homeUrl: '/',
url: '/idys/fyfilter.html',
filter_url: '{{fl.cateId}}{{fl.area}}{{fl.letter}}/page/fypage{{fl.year}}',
detailUrl: '',
searchUrl: '/idcz**/page/fypage.html',
searchable: 1, 
quickSearch: 1, 
filterable: 1, 

class_name: '电影&剧集&综艺&动漫&纪录',
class_url: 'dianying&dianshiju&zongyi&dongmna&jilupian',
filter_def: {
dianying: {cateId: 'dianying'},
dianshiju: {cateId: 'dianshiju'},
zongyi: {cateId: 'zongyi'},
dongmna: {cateId: 'dongmna'},
jilupian: {cateId: 'jilupian'}
},

tab_order: ['爱迪高清①', '爱迪影视②', '爱迪高清③'],
play_parse: true,
parse_url: 'https://api.lianfaka.com/vip/?url=',
lazy: `js:
var kcode = JSON.parse(request(input).match(/var player_.*?=(.*?)</)[1]);
kurl = unescape(base64Decode(kcode.url));
if (/m3u8|mp4/.test(kurl)) {
input = { jx: 0, parse: 0, url: kurl }
} else {
input = { jx: 0, parse: 1, url: rule.parse_url+kurl }
}`,

limit: 9,
double: false,
推荐: '*',
一级: '.lazyload;a&&title;a&&data-original;.text-right&&Text;a&&href',
二级: {
//名称;类型
title: 'h2.title&&Text;.data--span:eq(-2)&&Text',
//图片
img: '.v-thumb&&img&&data-original',
//主要描述;年份;地区;演员;导演
desc: '.v-thumb&&.text-right&&Text;.data--span:eq(-1)&&Text;.data--span:eq(-2)&&Text;.data--span:eq(0)&&Text;.data--span:eq(1)&&Text',
//简介
content: '.detail-content:eq(-1)&&Text',
//线路数组
tabs: '.bottom-line:has(span) h3',
//线路标题
tab_text: 'body&&Text',
//播放数组 选集列表
lists: '.stui-content__playlist:eq(#id)&&a',
//选集标题
list_text: 'body&&Text',
//选集链接
list_url: 'a&&href',
//链接处理
list_url_prefix: ''
},
搜索: '.v-thumb;*;*;*;*',

filter: 'H4sIAAAAAAAAA+2YS08TYRSG9/0Zs67hG5Drjvv9foewGKGBFqiIRQVCouEiBRFd0IrUiAlQSCSUgFyKlD/Tmdp/4dSZnjnfMQohLlycZd/nzPk6b/OFJ8y6lCGv5p/2+oeVkn7XrDLqmVZKlEEt4KkdUtyKXxv3mJ/15aXU8ZX5+Zk2NmUG/bOKPxMv7qfn9zOx+QH2zLlturKfvI6kgq+zAzMTwFLR9/rllcNGRxxmLH9IxoMOm0HPGa/eGS9D6LlHDtNDET0YddgLHzpv+diYX3SY9gQ9F4xKzJdhA3NuaEOb9Gioi0hMfxP/exfO7p1oenPJTnMyi3LsKDuR3ts0Lo6kCTuCHesx4zIh77AiaCUU01e29U9fpCEnhbnwrhH5Kg9ZEXybzwf61rX8bawIdpzE6IQdZSd+nK3oa2Fpwo7gjdZienxPfiMryk4kv68YEXmHHcHEzULqOmyE5OqcFH75xFv6fe3IeaON399oQ3qj1WM6YUfwRolzOmFHcMrCuvnL68sH8kGQwlm7N6n1w1RwUz4OUud2bRurN+bD8qGQwrmJE+uEZPyjfDQGsPX81DxKXmlF0o2Y9miTzo0wwmfp8Okdb0SuyH2Y3Z9Zk/MrQDSP0jxMcynNxVSlVMVUUCoQVYsJNQNEiygtwrSQ0kJMCygtwDSf0nxMaVcq7kqlXam4K5V2peKuVNqVirtSaVcq7krQrgTuStCuBO5K0K4E7krQrgTuStCuBO5K0K4E7krQrgTuStCuBO5K0K4E7krQrsxAui1jnkDAg+6Lfhg2jtbueF9Ks8utLTmlQMoIKQNSTkg5kApCKoBUElIJpIqQKiDVhFQDqSGkBkgtIbVA6gipA1JPSD2QBkIagDQS0gikiZAmIM2ENANpIaQFSCshrUDaCGkD0k5IO5AOQjqAdBLSCaSLkC4g3YR0A+khpAdILyG9QPoI6QMiHhQTlknMO+AacFuC+XTE65v6F4ZpLYI/V1vXyXjUFDh7YnjQ5/zhuzgyNQmxAGLh3YzWAJscQezbub4TctjIcx/rIOsg6yDrIOsg6yDrIOsg66Byfx2ceewfnvZiF2Shwupwuwyx6rDqsOqw6rDqsOqw6iDyv6nOkKk6436NXecPrsP/PGKjYqNio2KjYqNio2Kjut2ofN6xqQmv5melYqVipWKlYqVipWKlYqVipbqfUrnmfgLtsckxAjAAAA=='
}